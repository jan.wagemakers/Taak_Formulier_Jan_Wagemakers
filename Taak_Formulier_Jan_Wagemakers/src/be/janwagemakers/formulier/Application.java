/*
 * Oefening formulier
 * 
 * Maak een formulier in Java in een JFrame. 
 * Voeg een save knop toe die ervoor zorgt dat het formulier wordt weggeschreven in een tabel in je database. 
 * Maak dan het formulier leeg, zodat je een volgend item kan toevoegen.
 * 
 */
package be.janwagemakers.formulier;

import java.awt.EventQueue;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.Statement;

import javax.swing.GroupLayout;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JTextField;
import javax.swing.GroupLayout.Alignment;
import javax.swing.LayoutStyle.ComponentPlacement;
import javax.swing.border.EmptyBorder;
import javax.swing.ImageIcon;
import java.awt.Toolkit;

@SuppressWarnings("serial")
public class Application extends JFrame {
	// Hieronder de gegevens om met de database te verbinden.
	private String url="jdbc:mysql://localhost:3306/";
	private String user="root";
	private String pass="";
	private String database="Jan_Wagemakers_Emails"; // Naam van de database
	private String table="contacten"; 				 // Naam van de table

	// mysql : statement, preparedStatement, connection  
	private Statement statement;
	private PreparedStatement preparedStatement;
	private Connection connection;

	// GUI
	private JPanel contentPane;
	private JTextField txtVoornaam;
	private JTextField txtAchternaam;
	private JTextField txtEmail;

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					Application formulier = new Application();
					formulier.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	public Application() {
		initDatabase(); 	// 1. connectie maken met database
		createGUI();		// 2. GUI maken
	}

	/*
	 * initDatabase() :
	 * 	- Maak een connectie met de SQL-server
	 *  - maak database/velden aan indien ze nog niet bestaan
	 */
	public void initDatabase() {
		try{
			Class.forName("com.mysql.jdbc.Driver").newInstance();
			connection = DriverManager.getConnection(url,user,pass);

			//create statement
			statement = connection.createStatement();

			//create and select a database for use
			statement.execute("CREATE DATABASE IF NOT EXISTS " + database);
			statement.execute("USE " + database);

			//create an example table
			//statement.execute("DROP TABLE IF EXISTS " + table);	// database wissen, voor testdoeleinden.
			statement.execute("CREATE TABLE IF NOT EXISTS " + table + " ("
					+ "id BIGINT NOT NULL AUTO_INCREMENT,"
					+ "voornaam VARCHAR(25),"
					+ "achternaam VARCHAR(25),"
					+ "email VARCHAR(25),"
					+ "PRIMARY KEY(id)"
					+ ")");
		}
		catch(Exception e){
			e.printStackTrace();
			databaseError();
		}
	}

	/*
	 * save() : Deze methode wordt uitgevoerd als er op de save knop is geklikt.
	 */
	private void save() {
		System.out.println("Er is op de knop save gedrukt!");
		System.out.println("De gegevens zijn : " + txtVoornaam.getText() + " " + txtAchternaam.getText() + " " + txtEmail.getText());
		try {
			/* Input van gebruiker --> oppassen voor SQL-injection 
			 * statement.execute("INSERT INTO " + table + "(voornaam, achternaam, email)VALUES('" +
			 *		txtVoornaam.getText()   + "','" + 
			 *		txtAchternaam.getText() + "','" + 
			 *		txtEmail.getText()      + "')");
			 */

			// Doet hetzelfde als hierboven, maar (hopelijk) veilig voor SQL-injections
			preparedStatement = connection.prepareStatement("INSERT INTO " + table + "(voornaam, achternaam, email)VALUES(" +
					"? ," + 
					"? ," + 
					"? )");
			preparedStatement.setString(1,txtVoornaam.getText());
			preparedStatement.setString(2,txtAchternaam.getText());
			preparedStatement.setString(3,txtEmail.getText());
			preparedStatement.execute();
		}
		catch(Exception e){
			e.printStackTrace();
			databaseError();
		}
		txtVoornaam.setText("");		// Velden leeg maken voor de volgende input
		txtAchternaam.setText("");
		txtEmail.setText("");
		txtVoornaam.requestFocus();		// Cursor terug in eerste veld
	}

	/*
	 * dataBaseError() : Toon een bericht "Database Error!" op het scherm.
	 */
	private void databaseError() {
		JOptionPane.showMessageDialog(null, "Database Error!");
		System.exit(1);
	}

	/*
	 * createGUI() : maak de GUI aan. (Gemaakt met WindowBuilder)
	 */
	private void createGUI() {
		setTitle("Formulier");
		setIconImage(Toolkit.getDefaultToolkit().getImage(Application.class.getResource("/be/janwagemakers/formulier/images/save.png")));
		setResizable(false);
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 391, 273);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);

		JLabel lblVoornaam = new JLabel("Voornaam");

		JLabel lblAchternaam = new JLabel("Achternaam");

		JLabel lblEmail = new JLabel("Email");

		txtVoornaam = new JTextField();
		txtVoornaam.setText("");
		txtVoornaam.setColumns(10);

		txtAchternaam = new JTextField();
		txtAchternaam.setText("");
		txtAchternaam.setColumns(10);

		txtEmail = new JTextField();
		txtEmail.setText("");
		txtEmail.setColumns(10);

		JButton btnSave = new JButton("Save");
		btnSave.setIcon(new ImageIcon(Application.class.getResource("/be/janwagemakers/formulier/images/save.png")));
		btnSave.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				save();
			}
		});
		GroupLayout gl_contentPane = new GroupLayout(contentPane);
		gl_contentPane.setHorizontalGroup(
			gl_contentPane.createParallelGroup(Alignment.TRAILING)
				.addGroup(gl_contentPane.createSequentialGroup()
					.addGap(44)
					.addGroup(gl_contentPane.createParallelGroup(Alignment.TRAILING)
						.addComponent(lblEmail, Alignment.LEADING, GroupLayout.DEFAULT_SIZE, 58, Short.MAX_VALUE)
						.addComponent(lblVoornaam, GroupLayout.DEFAULT_SIZE, 58, Short.MAX_VALUE)
						.addComponent(lblAchternaam, GroupLayout.DEFAULT_SIZE, GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
					.addGap(18)
					.addGroup(gl_contentPane.createParallelGroup(Alignment.LEADING)
						.addGroup(gl_contentPane.createParallelGroup(Alignment.LEADING)
							.addComponent(txtAchternaam, Alignment.TRAILING, GroupLayout.PREFERRED_SIZE, 189, GroupLayout.PREFERRED_SIZE)
							.addComponent(txtVoornaam, Alignment.TRAILING, GroupLayout.PREFERRED_SIZE, 189, GroupLayout.PREFERRED_SIZE))
						.addComponent(txtEmail, GroupLayout.PREFERRED_SIZE, 189, GroupLayout.PREFERRED_SIZE)
						.addComponent(btnSave))
					.addGap(105))
		);
		gl_contentPane.setVerticalGroup(
			gl_contentPane.createParallelGroup(Alignment.LEADING)
				.addGroup(gl_contentPane.createSequentialGroup()
					.addGap(43)
					.addGroup(gl_contentPane.createParallelGroup(Alignment.BASELINE)
						.addComponent(lblVoornaam)
						.addComponent(txtVoornaam, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
					.addPreferredGap(ComponentPlacement.RELATED)
					.addGroup(gl_contentPane.createParallelGroup(Alignment.BASELINE)
						.addComponent(lblAchternaam)
						.addComponent(txtAchternaam, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
					.addPreferredGap(ComponentPlacement.RELATED)
					.addGroup(gl_contentPane.createParallelGroup(Alignment.BASELINE)
						.addComponent(lblEmail)
						.addComponent(txtEmail, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
					.addGap(38)
					.addComponent(btnSave)
					.addContainerGap(40, Short.MAX_VALUE))
		);
		contentPane.setLayout(gl_contentPane);
	}
}
